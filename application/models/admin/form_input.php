<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Model_CI for Login = Mohamad Apsyadira // devilcode25 
*/

class Form_input extends CI_Model {

	public function insert_data($data)
	{
		$this->db->insert('table_data',$data);
	}

	public function get_data()
	{
		return $this->db->get('table_data');
	}

	public function detail_id($where,$table)
	{
		return $this->db->get_where($table,$where);
	}

}