<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>asset/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/main.css">
    <title>Hello, world!</title>
</head>

<body>
    <div class="container">
        <h1>Selamat Datang Di Himpunan Pendidikan Informatika</h1>
        <h2>Mahasiswa yang sudah daftar</h2>
        <div class="data-mahasiswa" style="height: 450px; overflow: auto; margin-top: 50px;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">NIM</th>
                        <th scope="col">No. telp</th>
                        <th scope="col">Angkatan</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Sie</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=0; foreach($data as $s) { $no++;?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td><?=$s->nama_lengkap?></td>    
                        <td><?=$s->nim?></td>    
                        <td><?=$s->phone?></td>    
                        <td><?=$s->angkatan?></td>    
                        <td><?=$s->kelas?></td>    
                        <td><?=$s->sie?></td>    
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script> -->
</body>

</html>