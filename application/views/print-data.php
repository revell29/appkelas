<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url('asset/css/bootstrap.min.css');?>" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?=base_url('/asset/css/main.css');?>">
    <title>Hello, world!</title>
</head>

<body>
    <div class="container">
        <div class="data-mahasiswa" id="print-data">
            <h1>Biodata Peserta HMP</h1>
            <div class="col-md-6">
                <div class="foto">
                    3x4
                </div>
                <table class="table table-borderless">
                    <tr>
                        <th scope="col">Nama Lengkap</th>
                        <td><?php echo $this->input->get_post('nama_lengkap')?></td>
                    </tr>
                    <tr>
                        <th scope="col">NIM</th>
                        <td><?php echo $this->input->get_post('nim')?></td>
                    </tr>
                    <tr>
                        <th scope="col">No. Telp</th>
                        <td><?php echo $this->input->get_post('phone')?></td>
                    </tr>
                    <tr>
                        <th scope="col">Angkatan</th>
                        <td><?php echo $this->input->get_post('angkatan')?></td>
                    </tr>
                    <tr>
                        <th scope="col">Kelas</th>
                        <td><?php echo $this->input->get_post('kelas')?></td>
                    </tr>
                    <tr>
                        <th scope="col">Sie</th>
                        <td><?php echo implode(',',$this->input->get_post('sie'));?></td>
                    </tr>
                </table>
                <hr>
                <table class="table table-borderless">
                    <tr>
                        <th>Tanda Tangan Panitia</th>
                        <th>Tanda Tangan Peserta</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script> -->
</body>

</html>