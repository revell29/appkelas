<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url()?>asset/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/main.css">
    <title>Form Pendaftaran</title>
    <style type="text/css">
    #notifications {
    cursor: pointer;
    position: fixed;
    right: 0px;
    z-index: 9999;
    bottom: 0px;
    margin-bottom: 22px;
    margin-right: 15px;
    min-width: 300px; 
    max-width: 800px;  
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="col-md-4" id="content">
            <div class="form-daftar" id="daftar">
                <form method="POST" action="<?=base_url('action/input_data')?>">
                    <div class="form-group">
                        <label for="namalengkap">Nama Lengkap</label>
                        <input type="text" name="nama_lengkap" class="form-control" id="namalengkap" aria-describedby="namaHelp" placeholder="Masukan Nama">
                        <small id="namaHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="nim">NIM</label>
                        <input type="text" name="nim" class="form-control" id="nim" placeholder="Masukan NIM">
                    </div>
                    <div class="form-group">
                        <label for="nim">No. Telp.</label>
                        <input type="text" name="phone" class="form-control" id="nim" placeholder="Masukan Telp">
                    </div>
                    <div class="form-group">
                        <label for="angkatan">Angkatan</label>
                        <select class="custom-select" id="angkatan" name="angkatan">
                            <option selected>Pilih Angkatan</option>
                            <option value="2013">2013</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="custom-select" id="kelas" name="kelas">
                            <option selected>Pilih Kelas</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sie">Pilih Sie</label>
                        input
                        <div class="form-check" id="sie">
                            <input class="form-check-input" type="checkbox" id="exampleRadios2"  name="sie[]" value="SIE KESEKRETARIATAN">
                            <label class="form-check-label" for="exampleRadios2">
                                SIE KESEKRETARIATAN
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="exampleRadios2" name="sie[]" value="SIE PDD">
                            <label class="form-check-label" for="exampleRadios2">
                                SIE PDD
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="sie[]" value="SIE KONSUMSI">
                            <label class="form-check-label">
                                SIE KONSUMSI
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="sie[]" value="SIE KESEHATAN">
                            <label class="form-check-label">
                                SIE KESEHATAN
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="sie[]" value="SIE PERLENGKAPAN">
                            <label class="form-check-label">
                                SIE PERLENGKAPAN
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="SIE LO" name="sie[]">  
                            <label class="form-check-label">
                                SIE LO
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="SIE KOMDIS" name="sie[]">
                            <label class="form-check-label">
                                SIE KOMDIS
                            </label>
                        </div>
                        <small class="form-text text-muted">Maksimal hanya dapat memilih 2 Sie</small>
                    </div>
                    <div class="form-check">
                        <p>Jika tidak berkometment dengan baik maka akan di kenakan denda</p>
                        <input class="form-check-input" type="checkbox" value="agree" required>
                        <label class="form-check-label">
                            Saya Setuju
                        </label>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-primary btn-block" value="Kirim Data">
                </form>
            </div>
        </div>
    </div>
    <div id="notifications"><?php echo $this->session->flashdata('msg'); ?></div> 

    <script>   
    $('#notifications').slideDown('slow').delay(3000).slideUp('slow');
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script> -->
</body>

</html>