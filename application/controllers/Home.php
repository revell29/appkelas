<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'file'));
		$this->load->model('admin/form_input');
		$this->load->library('session');
		$this->load->library('upload');
	}

	public function index()
	{
		$this->load->view('halaman_data');
	}

	public function form_input()
	{
		$this->load->view('form-daftar');
	}

	public function panitia()
	{
		$data['data'] = $this->form_input->get_data()->result();
		$this->load->view('panitia/index',$data);
	}
}

