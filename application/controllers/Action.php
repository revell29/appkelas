<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'file'));
		$this->load->model('admin/form_input');
		$this->load->library('session');
		$this->load->library('upload');
	}

	public function input_data()
	{
		$data = array(
			'nama_lengkap' => $this->input->post('nama_lengkap'),
			'nim' => $this->input->post('nim'),
			'phone' => $this->input->post('phone'),
			'angkatan' => $this->input->post('angkatan'),
			'kelas' => $this->input->post('kelas'),
			'sie' => implode(',',$this->input->post('sie'))
		);

		if(!$this->form_input->insert_data($data))
		{
			$this->load->library('pdf');
				$this->pdf->setPaper('A4', 'potrait');
    			$this->pdf->filename = "data_user.pdf";
    			$this->pdf->load_view('print-data',$data);

		} else {
			$this->session->set_flashdata('msg','
				<div class="alert alert-danger">Gagal</div>
				');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}